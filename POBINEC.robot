*** Settings ***
Library  SeleniumLibrary
Resource  ../Resources/POB.robot
Resource  ../Resources/TestSetUp&TearDown.robot

Test Setup  TestSetUp&TearDown.Start TestCase
Test Teardown  TestSetUp&TearDown.Finish TestCase

*** Variables ***
${browser}  Chrome
${url}  https://cvr.inecnigeria.org/pu
${STValue}  31   #This is the number to be selected in the registration form
${LGValue}  648  #This is the number to be selected in the registration form
${RAValue}  7422   #This is the number to be selected in the registration form
${PUValue}  102303   #This is the number to be selected in the registration form

*** Test Cases ***
INEC Login Details
  Selection Of State  ${STValue}     #${STValue} indicate the argument number of state to be selected
  Selection Of Local Government Area  ${LGValue}    #${LGValue} indicate the argument number of Local Governemt Area to be selected
  Selection Of Registration Area  ${RAValue}   #${RAValue} indicate the argument number of Registration to be selected
  Selection Of polling unit  ${PUValue}  #${PUValue} indicate the argument number of Polling Unit to be selected
  Clicking Of Directions
