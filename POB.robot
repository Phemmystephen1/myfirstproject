*** Settings ***
Documentation  Testing for web browser
Library  SeleniumLibrary
Variables  ../PageObject/Locator.py
*** Keywords ***
Selection Of State
  [Arguments]  ${STValue}
  Page Should Contain Element  ${StateXpath}
  ${ListValue}=  Get Selected List Value  ${StateXpath}
  List Selection Should Be  ${StateXpath}  SELECT STATE
  Select From List By Value  ${StateXpath}  ${STValue}      #Selecting the state
  ${LLabel1}  Get Selected List Label  ${StateXpath}

Selection Of Local Government Area
  [Arguments]  ${LGValue}
  Page Should Contain Element  ${LGAXpath}
  ${ListValue}=  Get Selected List Value  ${LGAXpath}
  List Selection Should Be  ${LGAXpath}  --SELECT--
  Select From List By Value  ${LGAXpath}  ${LGValue}      #selecting the LGA
  ${LLabel2}  Get Selected List Label  ${LGAXpath}

Selection Of Registration Area
  [Arguments]  ${RAValue}
  Page Should Contain Element  ${RegistrationXpath}
  ${ListValue}=  Get Selected List Value  ${RegistrationXpath}
  List Selection Should Be  ${RegistrationXpath}  --SELECT--
  Select From List By Value  ${RegistrationXpath}  ${RAValue}      #selecting registration area
  ${LLabel3}  Get Selected List Label  ${RegistrationXpath}

Selection Of polling unit
  [Arguments]  ${PUValue}
  Page Should Contain Element  ${PollingXpath}
  ${ListValue}=  Get Selected List Value  ${PollingXpath}
  List Selection Should Be  ${PollingXpath}  --SELECT--
  Select From List By Value  ${PollingXpath}  ${PUValue}
  ${LLabel4}  Get Selected List Label  ${PollingXpath}
  Sleep  3s
Clicking Of Directions
  Click Element  ${DirecetionXpath}        #selecting get directions
  Sleep  10s
  ${path}=  Capture Page Screenshot

