*** Settings ***
Documentation  Testing for web browser
Library  SeleniumLibrary

*** Keywords ***
Start TestCase     #user defined keyword
  Open Browser  ${url}  ${browser}
  Maximize Browser Window
  Page Should Contain  Polling Unit Locator
Finish TestCase
  Close Browser

